namespace MancheJeu;
partial class Manche {
    public Int32 NumManche { get; private set; }
    private static Int32 num = 0;
    public Int32 NbJoueursManche { get; set; }
    public String ListeJoueursManche { get; set; }
    public String DateHeure { get; set; }
    public String EtatScores { get; set; }
    public String PointsJoueurs { get; set; }
    static Manche() {

    }

    public Manche() {
        NumManche = ++num;
        NbJoueursManche = default;
        ListeJoueursManche = "";
        DateHeure = "";
        EtatScores = "";
        PointsJoueurs = "";
    }

    public Manche(Int32 nbJoueurs, String liste) : this() {
        NbJoueursManche = nbJoueurs;
        ListeJoueursManche = liste;
    }
}